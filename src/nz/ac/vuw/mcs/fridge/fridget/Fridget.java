package nz.ac.vuw.mcs.fridge.fridget;

import java.net.MalformedURLException;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;

import nz.ac.vuw.mcs.fridge.backend.Fridge;
import nz.ac.vuw.mcs.fridge.backend.InterfridgeException;
import nz.ac.vuw.mcs.fridge.backend.model.AuthenticatedUser;
import nz.ac.vuw.mcs.fridge.backend.model.OrderLine;
import android.app.AlertDialog;
import android.app.TabActivity;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.DialogInterface.OnDismissListener;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.TabHost;
import android.widget.TextView;
import android.widget.TabHost.OnTabChangeListener;
import android.widget.TabHost.TabSpec;

public class Fridget extends TabActivity {
	/**
	 * Request ID for picking an item for the shopping list.
	 */
	private static final int PICK_FOR_SHOPPING_LIST = 1;

	/**
	 * Request ID for configuring the application.
	 */
	private static final int CONFIGURE = 2;

	/**
	 * Identifier for menu item.
	 */
	private static final int MENU_CONFIGURE = 1, MENU_REMOTE_FRIDGE = 2;

	private Fridge fridge;
	private AuthenticatedUser user;

	private List<OrderLine> shoppingList = new ArrayList<OrderLine>();
	private ShoppingListAdapter shoppingListAdapter;

	public Fridget() {
	}

	/** Called when the activity is first created. */
	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);

		FridgetApp app = ((FridgetApp) getApplication());
		if (app.getUser() == null) {
			try {
				app.login();
			}
			catch (MalformedURLException e) {
				e.printStackTrace();
				//Launch configuration activity
				launchConfigurator();
			}
			catch (InterfridgeException e) {
				e.printStackTrace();
				//Launch configuration activity
				launchConfigurator();
			}
			catch (ConfigurationMissingException e) {
				//Launch configuration activity
				launchConfigurator();
			}
		}
		fridge = app.getFridge();
		user = app.getUser();

		setContentView(R.layout.main);

		TabHost mTabHost = getTabHost();

		TabSpec a = mTabHost.newTabSpec("tab_account").setIndicator("Account").setContent(R.id.accountTab);
		mTabHost.addTab(a);
		mTabHost.addTab(mTabHost.newTabSpec("tab_purchase").setIndicator("Purchase").setContent(R.id.purchaseTab));
		mTabHost.addTab(mTabHost.newTabSpec("tab_transfer").setIndicator("Transfer").setContent(R.id.transferTab));
		
		mTabHost.setOnTabChangedListener(new OnTabChangeListener() {
			@Override
			public void onTabChanged(String tabId) {
				if (tabId.equals("tab_purchase") && shoppingList.size() == 0) {
					pickItem();
				}
			}
		});

		//Set up account tab
		refreshAccountTab();
		((Button) findViewById(R.id.topup)).setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				try {
					EditText topupAmount = (EditText) findViewById(R.id.topupAmount);
					int amount = (int) (Float.parseFloat(topupAmount.getText().toString()) * 100);
					if (doTopup(amount)) {
						//Topup was successful, so clear the text field
						topupAmount.setText("");
					}
				}
				catch (NumberFormatException e) {
					//Ignore, do not make topup
				}
			}
		});

		mTabHost.setCurrentTab(0);

		//Set up purchase tab
		ListView shoppingListView = (ListView) findViewById(R.id.shoppingList);
		shoppingListAdapter = new ShoppingListAdapter(this, shoppingList);
		shoppingListView.setAdapter(shoppingListAdapter);
		((Button) findViewById(R.id.addToShoppingList)).setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				pickItem();
			}
		});
		((Button) findViewById(R.id.purchase)).setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				if (shoppingList.size() > 0) {
					try {
						int orderPrice = user.purchase(fridge, shoppingList);
						//Notify user of success
						successDialog("Purchased for " + Fridge.formatMoney(orderPrice) + ".");
						//The order was successful, so clear the shopping list
						shoppingList.clear();
						shoppingListAdapter.notifyDataSetChanged();
						//User's balance will have changed
						refreshAccountTab();
					}
					catch (InterfridgeException e) {
						//Alert and quit
						e.printStackTrace();
						errorDialog(e.getMessage());
					}
				}
			}
		});

		//Set up transfer tab
		((Button) findViewById(R.id.transfer)).setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				try {
					EditText transferRecipient = (EditText) findViewById(R.id.transferRecipient);
					EditText transferAmount = (EditText) findViewById(R.id.transferAmount);
					String recipient = transferRecipient.getText().toString();
					int amount = (int) (Float.parseFloat(transferAmount.getText().toString()) * 100);
					if (doTransfer(recipient, amount)) {
						//Transfer was successful, so clear the text fields
						transferRecipient.setText("");
						transferAmount.setText("");
					}
				}
				catch (NumberFormatException e) {
					//Ignore, do not make transfer
				}
			}
		});
	}

	/**
	 * Utility method to launch FridgeItemPicker to pick an item to add to the shopping list.
	 */
	private void pickItem() {
		Intent intent = new Intent(getApplicationContext(), FridgeItemPicker.class);
		intent.setAction(FridgeItemPicker.ACTION_PICK_FRIDGE_ITEM);
		startActivityForResult(intent, PICK_FOR_SHOPPING_LIST);
	}

	/**
	 * Utility method to launch Configurator activity for the user to enter their login details.
	 */
	private void launchConfigurator() {
		Intent intent = new Intent(getApplicationContext(), Configurator.class);
		startActivityForResult(intent, CONFIGURE);
	}

	/**
	 * Utility method to prompt user for a remote fridge, then switch to it for future transactions.
	 */
	private void switchFridges() {
		try {
			AlertDialog.Builder builder = new AlertDialog.Builder(this);
			builder.setTitle("Remote fridge");
			builder.setNegativeButton("Cancel", null);
			Set<String> fridgesSet = user.getFridge().getRemoteFridges();
			final String[] fridges = fridgesSet.toArray(new String[fridgesSet.size() + 1]);
			fridges[fridges.length - 1] = "Home fridge (" + user.getFridge().getName() + ")";
			builder.setItems(fridges, new DialogInterface.OnClickListener() {
				@Override
				public void onClick(DialogInterface dialog, int which) {
					//Switch to requested fridge
					FridgetApp app = ((FridgetApp) getApplication());
					try {
						app.switchToFridge(which == fridges.length - 1 ? null : fridges[which]);
						fridge = app.getFridge();
						shoppingList.clear();
						shoppingListAdapter.notifyDataSetChanged();
						refreshAccountTab();
						System.out.println("Switched to remote fridge " + fridges[which]);
					}
					catch (InterfridgeException e) {
						//Alert and abort
						e.printStackTrace();
						successDialog(e.getMessage());
					}
				}
			});
			AlertDialog dialog = builder.show();
		}
		catch (InterfridgeException e) {
			//Alert and abort
			e.printStackTrace();
			successDialog(e.getMessage());
		}
	}

	private void refreshAccountTab() {
		TextView accountText = (TextView) findViewById(R.id.accountText);
		if (user == null) {
			accountText.setText("Not logged in.");
		}
		else {
			try {
				accountText.setText("Balance for " + user.getRealName() + " is " + Fridge.formatMoney(user.getBalance()) + ".\nUsing " + (user.getFridge().equals(fridge) ? "home" : "remote") + " fridge " + fridge.getName() + ".");
			}
			catch (InterfridgeException e) {
				//Show alert and quit
				e.printStackTrace();
				errorDialog(e.getMessage());
			}
		}
	}

	/**
	 * Utility method to show a dialog indicating success.
	 * @param message Message to display in the dialog.
	 */
	private void successDialog(String message) {
		AlertDialog.Builder builder = new AlertDialog.Builder(this);
		builder.setMessage(message);
		builder.setNeutralButton("Okay", null);
		builder.show();
	}

	/**
	 * Utility method to show a dialog indicating an error.
	 * @param message Message to display in the dialog.
	 */
	private void errorDialog(String message) {
		AlertDialog.Builder builder = new AlertDialog.Builder(this);
		builder.setTitle("Error");
		builder.setMessage(message);
		builder.setNeutralButton("Okay", null);
		AlertDialog dialog = builder.show();
		//Quit once dialog is dismissed
		dialog.setOnDismissListener(new OnDismissListener() {
			@Override
			public void onDismiss(DialogInterface dialog) {
				System.exit(1);
			}
		});
	}

	/**
	 * Make the topup (at the current fridge).
	 * @param amount Amount in cents by which to topup the user's account.
	 * @return True if successful.
	 */
	private boolean doTopup(int amount) {
		try {
			user.topup(fridge, amount);
			successDialog("Successfully topped up account by " + Fridge.formatMoney(amount) + " at " + fridge.getName());
			refreshAccountTab();
			return true;
		}
		catch (InterfridgeException e) {
			e.printStackTrace();
			successDialog("Error topping up by " + Fridge.formatMoney(amount) + ": " + e.getMessage());
		}
		return false;
	}

	/**
	 * Transfer fridge credit to another user.
	 * @param recipient Local or remote user to whom to transfer the given amount of credit.
	 * @param amount Amount in cents to transfer to the given recipient.
	 * @return True if successful.
	 */
	private boolean doTransfer(String recipient, int amount) {
		try {
			user.transfer(user.getFridge().parseUser(recipient), amount);
			successDialog("Successfully transferred " + Fridge.formatMoney(amount) + " to " + recipient + ".");
			refreshAccountTab();
			return true;
		}
		catch (InterfridgeException e) {
			e.printStackTrace();
			successDialog("Error transferring " + Fridge.formatMoney(amount) + " to " + recipient + ": " + e.getMessage());
		}
		catch (IllegalArgumentException e) {
			successDialog("Invalid recipient " + recipient + ": " + e.getMessage());
		}
		return false;
	}

	protected void onActivityResult(int requestCode, int resultCode, Intent data) {
		if (requestCode == PICK_FOR_SHOPPING_LIST && resultCode == RESULT_OK && data.getAction().equals(FridgeItemPicker.ACTION_PICK_FRIDGE_ITEM)) {
			//Item returned from FridgeItemPicker, add it to the shopping list
			String code = data.getData().getFragment();
			addToShoppingList(code);
		}
		else if (requestCode == CONFIGURE && resultCode == RESULT_OK) {
			//Configuration has changed, application has been told to login, so fetch fridge and user again
			FridgetApp app = ((FridgetApp) getApplication());
			fridge = app.getFridge();
			user = app.getUser();
			shoppingList.clear();
			shoppingListAdapter.notifyDataSetChanged();
			refreshAccountTab();
		}
	}

	/**
	 * Add one of the given item to the shopping list.
	 * If there are none in the list it will be added to the end; if there are already some then the quantity will be incremented.
	 * @param code The fridge code of the item to add.
	 */
	private void addToShoppingList(String code) {
		addItem: {
			for (OrderLine line: shoppingList) {
				if (line.getCode().equals(code)) {
					//Found item in list, just increment quantity
					line.incrementQty(1);
					break addItem;
				}
			}
			//Item not in list, add it
			shoppingList.add(new OrderLine(code, 1));
		}
		//Refresh ListView
		shoppingListAdapter.notifyDataSetChanged();
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		super.onCreateOptionsMenu(menu);
		menu.add(Menu.NONE, MENU_CONFIGURE, Menu.NONE, "Configure");
		menu.add(Menu.NONE, MENU_REMOTE_FRIDGE, Menu.NONE, "Remote fridge");
		return true;
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		switch (item.getItemId()) {
			case MENU_CONFIGURE:
				launchConfigurator();
				return true;
			case MENU_REMOTE_FRIDGE:
				switchFridges();
				return true;
		}
		return super.onOptionsItemSelected(item);
	}
}
