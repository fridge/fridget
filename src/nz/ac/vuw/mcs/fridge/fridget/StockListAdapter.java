package nz.ac.vuw.mcs.fridge.fridget;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import nz.ac.vuw.mcs.fridge.backend.Fridge;
import nz.ac.vuw.mcs.fridge.backend.model.StockItem;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

public class StockListAdapter extends BaseAdapter {
	private final int ITEM_VIEW_TYPE = 0;
	private final int CATEGORY_HEADER_VIEW_TYPE = 1;

	private final Context context;

	/**
	 * List of categories, in the order in which they should be displayed.
	 */
	private List<String> categories = new ArrayList<String>();

	/**
	 * Map from category to sorted list of items.
	 */
	private Map<String, List<StockItem>> categorisedItems = new HashMap<String, List<StockItem>>();

	private LayoutInflater inflater;

	public StockListAdapter(Context context, Collection<StockItem> stock) {
		this.context = context;

		inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);

		//Sort stock items into categories
		for (StockItem item: stock) {
			if (!categorisedItems.containsKey(item.category)) {
				categorisedItems.put(item.category, new ArrayList<StockItem>());
				while (categories.size() < item.categoryOrder) {
					categories.add(null);
				}
				categories.set(item.categoryOrder - 1, item.category);
			}
			categorisedItems.get(item.category).add(item);
		}
	}

	public boolean areAllItemsEnabled() {
		return false;
	}

	public boolean isEnabled(int position) {
		//Category headers are not enabled, but all items are
		return getItemViewType(position) == ITEM_VIEW_TYPE;
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		//Re-use convertView if suitable
		TextView tv = null;
		if (convertView instanceof TextView) {
			tv = (TextView) convertView;
		}

		Object item = getItem(position);
		if (item instanceof String) {
			//Category header
			if (tv == null || !tv.getTag().equals("pickercategory")) {
				tv = (TextView) inflater.inflate(R.layout.pickercategory, parent, false);
			}
			tv.setText((String) item);
		}
		else {
			//Stock item
			//FIXME: For some reason reusing the old TextView does not work, setTextAppearance is ignored. To work around this we construct a new TextView each time.
			//if (tv == null || !tv.getTag().equals("pickeritem")) {
				tv = (TextView) inflater.inflate(R.layout.pickeritem, parent, false);
			//}
			StockItem stockItem = (StockItem) item;
			tv.setText(stockItem.productCode + ": " + stockItem.description + " " + Fridge.formatMoney(stockItem.price) + " (" + stockItem.inStock + ")");
			if (stockItem.inStock > 0) {
				tv.setTextAppearance(context, R.style.inStockText);
			}
			else if (stockItem.inStock == 0) {
				tv.setTextAppearance(context, R.style.noStockText);
			}
			else {
				tv.setTextAppearance(context, R.style.negativeStockText);
			}
		}
		return tv;
	}

	@Override
	public int getItemViewType(int position) {
		for (int category = 0; category < categories.size(); ++category) {
			if (position == 0) {
				return CATEGORY_HEADER_VIEW_TYPE;
			}
			position -= categorisedItems.get(categories.get(category)).size() + 1;
			if (position < 0) {
				return ITEM_VIEW_TYPE;
			}
		}
		throw new IndexOutOfBoundsException();
	}

	@Override
	public int getViewTypeCount() {
		return 2;
	}

	public Object getItem(int position) {
		for (int category = 0; category < categories.size(); ++category) {
			if (position == 0) {
				return categories.get(category);
			}
			--position;
			List<StockItem> categoryItems = categorisedItems.get(categories.get(category));
			int numItemsInCategory = categoryItems.size();
			if (position < numItemsInCategory) {
				return categoryItems.get(position);
			}
			position -= numItemsInCategory;
		}
		throw new IndexOutOfBoundsException();
	}

	public int getCount() {
		int numCategories = categories.size();
		int count = numCategories;
		for (int i = 0; i < numCategories; ++i) {
			count += categorisedItems.get(categories.get(i)).size();
		}
		return count;
	}

	@Override
	public long getItemId(int position) {
		return position;
	}
}
