package nz.ac.vuw.mcs.fridge.fridget;

import java.util.List;

import nz.ac.vuw.mcs.fridge.backend.model.OrderLine;
import android.content.Context;
import android.view.View;
import android.view.ViewGroup;
import android.view.View.OnFocusChangeListener;
import android.widget.ArrayAdapter;
import android.widget.TextView;

public class ShoppingListAdapter extends ArrayAdapter<OrderLine> {
	public ShoppingListAdapter(Context context, List<OrderLine> shoppingList) {
		super(context, R.layout.orderline, R.id.orderLineDescription, shoppingList);
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		View view = super.getView(position, convertView, parent);

		final OrderLine orderLine = getItem(position);

		TextView description = (TextView) view.findViewById(R.id.orderLineDescription);
		description.setText(orderLine.getCode());

		TextView quantity = (TextView) view.findViewById(R.id.orderLineQuantity);
		quantity.setText(Integer.toString(orderLine.getQty()));
		quantity.setOnFocusChangeListener(new OnFocusChangeListener() {
			@Override
			public void onFocusChange(View v, boolean hasFocus) {
				if (!hasFocus) {
					try {
						int quantity = Integer.parseInt(((TextView) v).getText().toString());
						if (quantity == 0) {
							remove(orderLine);
						}
						else {
							orderLine.setQty(quantity);
						}
					}
					catch (NumberFormatException e) {
						//Ignore invalid value, replace it with original value
						((TextView) v).setText(Integer.toString(orderLine.getQty()));
					}
				}
			}
		});

		return view;
	}
}
