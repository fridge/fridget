package nz.ac.vuw.mcs.fridge.fridget;

import java.util.Map;

import nz.ac.vuw.mcs.fridge.backend.Fridge;
import nz.ac.vuw.mcs.fridge.backend.InterfridgeException;
import nz.ac.vuw.mcs.fridge.backend.model.StockItem;
import nz.ac.vuw.mcs.fridge.backend.model.User;
import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.DialogInterface.OnDismissListener;
import android.net.Uri;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;

public class FridgeItemPicker extends Activity {
	public static final String ACTION_PICK_FRIDGE_ITEM = "nz.ac.vuw.mcs.fridge.fridget.PICK_FRIDGE_ITEM";

	private StockListAdapter adapter;

	public FridgeItemPicker() {
	}

	/** Called when the activity is first created. */
	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);

		setContentView(R.layout.itempicker);

		((ListView) findViewById(R.id.itempicker)).setOnItemClickListener(new ListView.OnItemClickListener() {
			@Override
			public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
				Object listItem = adapter.getItem(position);
				if (listItem instanceof StockItem) {
					String code = ((StockItem) listItem).productCode;
					Intent data = new Intent(ACTION_PICK_FRIDGE_ITEM, Uri.fromParts("fridge", "item", code));
					setResult(RESULT_OK, data);
					finish();
				}
			}
		});
	}

	@Override
	public void onStart() {
		super.onStart();

		FridgetApp app = ((FridgetApp) getApplication());
		Fridge fridge = app.getFridge();
		User user = app.getUser();

		if (fridge != null) {
			ListView stockList = (ListView) findViewById(R.id.itempicker);
			try {
				Map<String, StockItem> stock = fridge.getStockForUser(user);
				adapter = new StockListAdapter(this, stock.values());
				stockList.setAdapter(adapter);
			}
			catch (InterfridgeException e) {
				//Alert and quit
				e.printStackTrace();
				errorDialog(e.getMessage());
			}
		}
	}

	/**
	 * Utility method to show a dialog indicating an error.
	 * @param message Message to display in the dialog.
	 */
	private void errorDialog(String message) {
		AlertDialog.Builder builder = new AlertDialog.Builder(this);
		builder.setTitle("Error");
		builder.setMessage(message);
		builder.setNeutralButton("Okay", null);
		AlertDialog dialog = builder.show();
		//Quit once dialog is dismissed
		dialog.setOnDismissListener(new OnDismissListener() {
			@Override
			public void onDismiss(DialogInterface dialog) {
				finish();
			}
		});
	}
}
