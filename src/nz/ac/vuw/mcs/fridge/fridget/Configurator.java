package nz.ac.vuw.mcs.fridge.fridget;

import java.net.MalformedURLException;

import nz.ac.vuw.mcs.fridge.backend.InterfridgeException;
import android.app.Activity;
import android.app.AlertDialog;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.EditText;

/**
 * This activity allows the user to enter their fridge and login details.
 */
public class Configurator extends Activity {
	/** Called when the activity is first created. */
	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);

		setContentView(R.layout.configurator);

		((Button) findViewById(R.id.saveConfiguration)).setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				String fridgeEndpoint = ((EditText) findViewById(R.id.fridgeEndpoint)).getText().toString();
				String username = ((EditText) findViewById(R.id.username)).getText().toString();
				String password = ((EditText) findViewById(R.id.password)).getText().toString();

				SharedPreferences preferences = getSharedPreferences(FridgetApp.PREFERENCES_NAME, MODE_PRIVATE);
				SharedPreferences.Editor editor = preferences.edit();
				editor.putString("fridgeEndpoint", fridgeEndpoint);
				editor.putString("username", username);
				editor.putString("password", password);
				editor.commit();

				FridgetApp app = ((FridgetApp) getApplication());
				try {
					app.login();
				}
				catch (MalformedURLException e) {
					dialog("Invalid fridge endpoint: " + e.getMessage());
					return;
				}
				catch (InterfridgeException e) {
					dialog("Error logging in: " + e);
					return;
				}
				catch (ConfigurationMissingException e) {
					dialog("Configuration missing.");
					return;
				}
				setResult(RESULT_OK);
				finish();
			}
		});
	}

	@Override
	public void onStart() {
		super.onStart();

		SharedPreferences preferences = getSharedPreferences(FridgetApp.PREFERENCES_NAME, MODE_PRIVATE);
		((EditText) findViewById(R.id.fridgeEndpoint)).setText(preferences.getString("fridgeEndpoint", null));
		((EditText) findViewById(R.id.username)).setText(preferences.getString("username", null));
		((EditText) findViewById(R.id.password)).setText(preferences.getString("password", null));
	}

	/**
	 * Utility method to show a dialog.
	 * @param message Message to display in the dialog.
	 */
	private void dialog(String message) {
		AlertDialog.Builder builder = new AlertDialog.Builder(this);
		builder.setMessage(message);
		builder.setNeutralButton("Okay", null);
		builder.show();
	}
}
