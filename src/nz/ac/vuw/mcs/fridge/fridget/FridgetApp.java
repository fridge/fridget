package nz.ac.vuw.mcs.fridge.fridget;

import java.net.MalformedURLException;

import nz.ac.vuw.mcs.fridge.backend.Fridge;
import nz.ac.vuw.mcs.fridge.backend.InterfridgeException;
import nz.ac.vuw.mcs.fridge.backend.model.AuthenticatedUser;
import android.app.Application;
import android.content.SharedPreferences;

public class FridgetApp extends Application {
	public static final String PREFERENCES_NAME = "fridgeLogin";

	private Fridge fridge;
	private AuthenticatedUser user;

	/**
	 * Login with the details from the application preferences file, and initialise the fridge and user objects.
	 * @throws MalformedURLException If the fridge endpoint URL is invalid.
	 * @throws InterfridgeException If there is a problem logging in.
	 */
	public void login() throws MalformedURLException, InterfridgeException, ConfigurationMissingException {
		SharedPreferences preferences = getSharedPreferences(PREFERENCES_NAME, MODE_PRIVATE);
		String fridgeEndpoint = preferences.getString("fridgeEndpoint", null);
		String username = preferences.getString("username", null);
		String password = preferences.getString("password", null);
		if (fridgeEndpoint == null || username == null || password == null) {
			throw new ConfigurationMissingException();
		}

		fridge = new Fridge(fridgeEndpoint);
		user = fridge.authenticateUser(username, password);
	}

	public Fridge getFridge() {
		return fridge;
	}

	public AuthenticatedUser getUser() {
		return user;
	}

	/**
	 * Switch the current fridge to the given remote fridge peered with the user's home fridge.
	 * @param name The name of the desired remote fridge, or null for the user's home fridge.
	 * @throws InterfridgeException
	 */
	public void switchToFridge(String name) throws InterfridgeException {
		if (name == null) {
			fridge = user.getFridge();
		}
		else {
			fridge = user.getFridge().getRemoteFridge(name);
		}
	}
}
