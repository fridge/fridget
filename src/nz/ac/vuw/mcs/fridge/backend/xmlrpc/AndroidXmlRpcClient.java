package nz.ac.vuw.mcs.fridge.backend.xmlrpc;

import nz.ac.vuw.mcs.fridge.backend.xmlrpc.GenericXmlRpcClient;

import org.xmlrpc.android.XMLRPCClient;
import org.xmlrpc.android.XMLRPCException;

public class AndroidXmlRpcClient implements GenericXmlRpcClient {
	private final XMLRPCClient client;

	public AndroidXmlRpcClient(String endpoint) {
		client = new XMLRPCClient(endpoint);
	}

	public Object execute(String method, Object[] arguments) throws GenericXmlRpcException {
		try {
			return client.callEx(method, arguments);
		}
		catch (XMLRPCException e) {
			throw new GenericXmlRpcException(e);
		}
	}
}
